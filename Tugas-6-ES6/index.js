//soal 1
let panjang = 10;
let lebar = 5;
//luas
const menghitung_luas = () => {
    //function
    console.log(panjang * lebar);
}
//keliling
const menghitung_keliling = () => {
    //function
    console.log((panjang *2)+ (lebar*2));
}
// panggil Function
menghitung_luas()
menghitung_keliling()

//soal 2

let firstName = "William";
let lastName = "Imoh";
const newFunction = () =>{
    return {
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction().fullName() 

  //soal3
var studentName = {   namapertama: 'Muhammad',namaterakhir: 'iqbal mubarok',alamat: 'jalan ranamanyar',hobby: 'playing football'};
const {namapertama, namaterakhir, alamat , hobby } = studentName;
console.log(namapertama, namaterakhir, alamat, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
 
let combined = [west, east]
console.log(combined)

//soal5
const planet = "earth" 
const view = "glass"
const string = `lorem ${view} dolor sit amet, dolor sit amet, ${planet}`
console.log(string)