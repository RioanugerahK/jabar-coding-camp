//soal 1
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function memanggil(data) {
    readBooks(data, books[1], function memanggil(data) {
        readBooks(data, books[2], function memanggil(data) {
            readBooks(data, books[3], function memanggil(data){})
        });
    });
});
